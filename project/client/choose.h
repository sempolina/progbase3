#ifndef CHOOSE_H
#define CHOOSE_H

#include <QDialog>

namespace Ui {
class Choose;
}

class Choose : public QDialog
{
    Q_OBJECT
    bool islogin_=false;
public:
    explicit Choose(QWidget *parent = 0);
    bool data();
    ~Choose();
signals:
    void signalFromButton(bool islogin);
private slots:
    void set(bool islogin);
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::Choose *ui;
};

#endif // CHOOSE_H
