#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include <QDialog>
#include"cat.h"
using namespace std;
namespace Ui {
class AddDialog;
}

class AddDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddDialog(QWidget *parent = 0);
    ~AddDialog();
    Cat data();
private slots:
    void on_pushButton_clicked();

private:
    Ui::AddDialog *ui;
};

#endif // ADDDIALOG_H
