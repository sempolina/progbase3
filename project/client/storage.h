#ifndef STORAGE_H
#define STORAGE_H

#include<vector>
#include "cat.h"
#include "breed.h"
#include"user.h"
#include <optional>
using namespace  std;

class Storage
{
private:
    string dir_name_;
public:
    Storage(){}
    explicit Storage(const string & dir_name=""){dir_name_ = dir_name;}
    virtual ~Storage(){}
    void setName(const string & dir_name);
    string name() const;

     virtual bool isOpen() const=0;
    virtual bool open()=0;
     virtual void close()=0;

     virtual vector<Cat> getAllCats()=0;
     virtual optional<Cat> getCatById(int cat_id)=0;
     virtual bool updateCat(const Cat & cat)=0;
     virtual bool removeCat(int cat_id)=0;
     virtual int insertCat(const Cat & cat)=0;

     virtual vector<Breed> getAllBreeds()=0;
     virtual optional<Breed> getBreedById(int breed_id)=0;
     virtual bool updateBreed(const Breed & breed)=0;
     virtual bool removeBreed(int breed_id)=0;
     virtual int insertBreed(const Breed & breed)=0;

     virtual int registerUser(QString const & username,QString const & password, QString const & fullname)=0;
     virtual optional<User> getUserAuth(QString const & username, QString const & password)=0;
     virtual vector<Cat> getAllUserCats(int user_id)=0;

    virtual vector<Breed> getAllCatBreeds(int cat_id)=0;
    virtual bool insertCatBreeds(int cat_id,int breed_id)=0;
    virtual bool removeCatBreed(int cat_id,int breed_id)=0;

    virtual vector<Cat> search(QString const & s,int use_id,int number,int miss)=0;
    virtual int number(QString const & s,int use_id)=0;
};

#endif // STORAGE_H
