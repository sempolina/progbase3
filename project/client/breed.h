#ifndef ALBUM_H
#define ALBUM_H

#include<string>
#include <iostream>
#include <QListWidgetItem>
using namespace std;
struct Breed
{
    int id;
    string name;
    string size;
    int age_of_life;
    Breed(){}
    Breed(const Breed & al){this->id=al.id;this->name=al.name;this->size=al.size;this->age_of_life=al.age_of_life;}
};
Q_DECLARE_METATYPE(Breed)
#endif // ALBUM_H
