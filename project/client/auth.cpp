#include "auth.h"
#include "ui_auth.h"

Auth::Auth(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Auth)
{
    ui->setupUi(this);
    ui->lineEdit_2->setEchoMode(QLineEdit::Password);
}
User Auth::data()
{
 User user;
 user.username=ui->lineEdit->text();
 user.hash_password=ui->lineEdit_2->text();
 return user;
}
Auth::~Auth()
{
    delete ui;
}
