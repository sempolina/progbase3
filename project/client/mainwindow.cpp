#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QSqlError>
#include "adddialog.h"
#include "edditdialog.h"
#include <QMessageBox>
#include<QDebug>
#include "adddialog2.h"
#include "auth.h"
#include"choose.h"
#include"register.h"
#include <QFile>
#include "imp_exp.h"
using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->actionOpen,&QAction::triggered,this,&MainWindow::onOpen);
    connect(ui->actionExit,&QAction::triggered,this,&MainWindow::beforeClose);
    connect(ui->actionlogout,&QAction::triggered,this,&MainWindow::onLogout);
    connect(ui->actionImport_csv,&QAction::triggered,this,&MainWindow::onImport);
    connect(ui->actionExport_csv,&QAction::triggered,this,&MainWindow::onExport);
}

MainWindow::~MainWindow()
{
    if(storage_!=nullptr)
        delete storage_;
    delete ui;
}
void MainWindow::addCats(const vector<Cat> & cats)
{
    for(const Cat & cat:cats)
    {
        QVariant qVar=QVariant::fromValue(cat);
        QListWidgetItem * qCatListItem=new QListWidgetItem();
        qCatListItem->setText(QString::fromStdString(cat.name));
        qCatListItem->setData(Qt::UserRole,qVar);
        ui->listWidget->addItem(qCatListItem);
    }
}
void MainWindow::enter()
{
    Choose ch(this);
    if(ch.exec())
    {
        if(ch.data())
         {
            User user;
           Auth auth(this);
           if(auth.exec())
           {
               user=auth.data();
           }
           else
           {
              return;
           }
           while (storage_->getUserAuth(user.username,user.hash_password)==nullopt)
           {
               QMessageBox::warning(this,"Authentication","Incorrect login or password. Try again");
               Auth auth2(this);
               if(auth2.exec())
               {
                   user=auth2.data();
               }
               else
               {
                   return;
               }
           }
           user_=storage_->getUserAuth(user.username,user.hash_password).value();
           ui->add_button->setEnabled(true);
           ui->search->setEnabled(true);
           ui->label_2->setText(user_.fullname);
           ui->next->setEnabled(true);
           ui->previous->setEnabled(true);
           vector<Cat> cats=storage_->search(serc,user_.id,on_page,0);
           page=1;
           max_page=1;
           int coun=storage_->number(serc,user_.id);
           while(coun/on_page!=0)
           {
               max_page++;
               coun=coun/on_page;
           }
           ui->pages->setText("page:"+QString::number(page)+"/"+QString::number(max_page));
           this->addCats(cats);
        }
        else
        {
            User user;
            Register reg;
            if(reg.exec())
            {
                user=reg.data();
            }
            else
            {
                return;
            }
            while (storage_->registerUser(user.username,user.hash_password,user.fullname)==-1)
            {
                QMessageBox::warning(this,"Authentication","Username already exist.");
                Register reg2(this);
                if(reg2.exec())
                {
                    user=reg2.data();
                }
                else
                {
                    return;
                }
            }
            user_=user;
            ui->add_button->setEnabled(true);
            ui->search->setEnabled(true);
            ui->label_2->setText(user_.fullname);
            vector<Cat> cats=storage_->search(serc,user_.id,on_page,0);
            this->addCats(cats);
        }
    }
}
void MainWindow::onOpen()
{
   QString fileName=QFileDialog::getOpenFileName(this,"Dialog Caption","","SQLITE (*.sqlite)");
   if(!fileName.isEmpty()&&!fileName.isNull())
   {
       if(storage_!=nullptr)
       {
           delete storage_;
           ui->listWidget->clear();
           ui->listWidget_2->clear();
       }
       storage_=new SqliteStorage(fileName.toStdString());
       ui->label_2->setText("");
       ui->selected->setText("");
       ui->selected_2->setText("");
       ui->remove_button->setEnabled(false);
       ui->edit_button->setEnabled(false);
       ui->remove_button_2->setEnabled(false);
       ui->add_button_2->setEnabled(false);
       ui->add_button->setEnabled(false);
       ui->next->setEnabled(false);
       ui->previous->setEnabled(false);
       this->enter();
   }
}

void MainWindow::addBreeds(const vector<Breed> & breeds)
{
    for(const Breed & breed:breeds)
    {
        QVariant qVar=QVariant::fromValue(breed);
        QListWidgetItem * qALbumListItem=new QListWidgetItem();
        qALbumListItem->setText(QString::fromStdString(breed.name));
        qALbumListItem->setData(Qt::UserRole,qVar);
        ui->listWidget_2->addItem(qALbumListItem);
    }
}
void MainWindow::on_listWidget_itemClicked(QListWidgetItem *item)
{
        ui->listWidget_2->clear();
        ui->selected_2->setText("");
        ui->remove_button->setEnabled(true);
        ui->edit_button->setEnabled(true);
        ui->remove_button_2->setEnabled(false);
        ui->add_button_2->setEnabled(true);
    Cat cat= item->data(Qt::UserRole).value<Cat>();
    vector<Breed> breeds=storage_->getAllCatBreeds(cat.id);
    this->addBreeds(breeds);
    ui->selected->setText("Selected Cat:\nId:"+QString::number(cat.id)+
                          "\nname:"+ QString::fromStdString(cat.name)+
                          "\nage:"+QString::number(cat.age)+" months\nweight:"+QString::number(cat.weight,'f',1)+" kg");
    QPixmap pix(QDir::currentPath()+QString::fromStdString(cat.photo));
    ui->label->setPixmap(pix);
}

void MainWindow::on_remove_button_clicked()
{
    QMessageBox::StandardButton reply;
    reply=QMessageBox::question(this,"On remove","Are you sure?");
    if  (reply==QMessageBox::StandardButton::Yes)
    {
            ui->selected->setText("");
            ui->label->clear();
           ui->remove_button->setEnabled(false);
           ui->edit_button->setEnabled(false);
        ui->selected_2->setText("");
        QList <QListWidgetItem*> items=ui->listWidget->selectedItems();
        Cat cat;
        foreach(QListWidgetItem *item,items)
        {
            cat=item->data(Qt::UserRole).value<Cat>();
            storage_->removeCat(cat.id);
            delete ui->listWidget->takeItem(ui->listWidget->row(item));
        }
        ui->listWidget->clear();
            ui->listWidget_2->clear();
            ui->label_2->setText("");
            ui->add_button_2->setEnabled(false);
            ui->remove_button_2->setEnabled(false);
            vector<Cat> cats=storage_->search(serc,user_.id,on_page,on_page*(page-1));
            max_page=1;
            int coun=storage_->number(serc,user_.id);
            while(coun/on_page!=0)
            {
                max_page++;
                coun=coun/on_page;
            }
            ui->pages->setText("page:"+QString::number(page)+"/"+QString::number(max_page));
            this->addCats(cats);

    }
}

void MainWindow::on_add_button_clicked()
{

    AddDialog add(this);
    int status=add.exec();
    if(status==1)
    {
        Cat cat=add.data();
        qDebug()<<QDir::currentPath()+QString::fromStdString(cat.photo);
        if(!QFile(QDir::currentPath()+QString::fromStdString(cat.photo)).exists())
        {

              QMessageBox::warning(this,"Addition","Incorrect way to photo. Addition cat is failed.");
        }
        else
        {
            cat.user_id=user_.id;
            cat.id=storage_->insertCat(cat);
            ui->listWidget->clear();
            vector<Cat> cats=storage_->search(serc,user_.id,on_page,on_page*(page-1));
            max_page=1;
            int coun=storage_->number(serc,user_.id);
            while(coun/on_page!=0)
            {
                max_page++;
                coun=coun/on_page;
            }
            ui->pages->setText("page:"+QString::number(page)+"/"+QString::number(max_page));
            this->addCats(cats);
            ui->selected->setText("");
            ui->label->clear();
           ui->remove_button->setEnabled(false);
           ui->edit_button->setEnabled(false);
        ui->selected_2->setText("");
        ui->listWidget_2->clear();
        ui->label_2->setText("");
        ui->add_button_2->setEnabled(false);
        ui->remove_button_2->setEnabled(false);
        }
    }
    else
    {

    }
}

void MainWindow::on_edit_button_clicked()
{
    QList <QListWidgetItem*> items=ui->listWidget->selectedItems();
    Cat cat;
        foreach(QListWidgetItem *item,items)
        {
            cat=item->data(Qt::UserRole).value<Cat>();
            EdditDialog edit(this);
            edit.setData(cat);
            int status=edit.exec();
            if(status==1)
            {
                if(!QFile(QDir::currentPath()+QString::fromStdString(cat.photo)).exists())
                {

                      QMessageBox::warning(this,"Edit","Incorrect way to photo. Edition cat is failed.");
                }
                else
                {
                    Cat c=edit.data();
                    c.id=cat.id;
                    storage_->updateCat(c);
                    QVariant qVar=QVariant::fromValue(c);
                    item->setData(Qt::UserRole,qVar);
                    item->setText(QString::fromStdString(c.name));
                    ui->listWidget->editItem(item);
                    ui->selected->setText("Selected Cat:\nId:"+QString::number(c.id)+
                                          "\nname:"+ QString::fromStdString(c.name)+
                                          "\nage:"+QString::number(c.age)+" months\nweight:"+QString::number(c.weight,'f',1)+" kg");
                    QPixmap pix(QDir::currentPath()+QString::fromStdString(c.photo));
                    ui->label->setPixmap(pix);
                }
            }

        }
}
void MainWindow::beforeClose()
{
    QMessageBox::StandardButton reply;
    reply=QMessageBox::question(this,"On exit","Are you sure?");
    if  (reply==QMessageBox::StandardButton::Yes)
    {
        this->close();
    }
}

void MainWindow::on_listWidget_2_itemClicked(QListWidgetItem *item)
{
    ui->remove_button_2->setEnabled(true);
Breed al= item->data(Qt::UserRole).value<Breed>();
ui->selected_2->setText("Selected Breed:\nId:"+QString::number(al.id)+
                      "\nname:"+ QString::fromStdString(al.name)+
                      "\nsize:"+QString::fromStdString(al.size)+"\nage of life:"+QString::number(al.age_of_life)+"years");
}

void MainWindow::on_add_button_2_clicked()
{
    QList <QListWidgetItem*> items=ui->listWidget->selectedItems();
    Cat cat;
    foreach(QListWidgetItem *item,items)
    {
        cat=item->data(Qt::UserRole).value<Cat>();
     }
    vector<Breed> breeds;
    vector<Breed> all_al=storage_->getAllBreeds();
    vector<Breed> cat_al=storage_->getAllCatBreeds(cat.id);
    for(int i=0;i<all_al.size();i++)
    {
        bool exist=false;
        for(int j=0;j<cat_al.size();j++)
        {
            if(all_al[i].id==cat_al[j].id)
            {
                exist=true;
                break;
            }
        }
        if(!exist)
        {
            breeds.push_back(all_al[i]);
        }
    }
    adddialog2 add(this);
    add.setData(breeds);
    int status=add.exec();
    if(status==1)
    {
        Breed breed=add.data();
        storage_->insertCatBreeds(cat.id,breed.id);
        QVariant qVar=QVariant::fromValue(breed);
        QListWidgetItem * qBreedListItem=new QListWidgetItem();
        qBreedListItem->setText(QString::fromStdString(breed.name));
        qBreedListItem->setData(Qt::UserRole,qVar);
        ui->listWidget_2->addItem(qBreedListItem);
    }
    else
    {

    }
}

void MainWindow::on_remove_button_2_clicked()
{
    QMessageBox::StandardButton reply;
    reply=QMessageBox::question(this,"On remove","Are you sure?");
    if  (reply==QMessageBox::StandardButton::Yes)
    {

        QList <QListWidgetItem*> items=ui->listWidget_2->selectedItems();
        Breed al;
        al=items[0]->data(Qt::UserRole).value<Breed>();
        QList <QListWidgetItem*> items2=ui->listWidget->selectedItems();
        Cat cat=items2[0]->data(Qt::UserRole).value<Cat>();
        try
        {storage_->removeCatBreed(cat.id,al.id);}
        catch(string i)
        {
            cout<<i;
        }
        catch(QSqlError error)
        {
            qDebug()<<error.text();
        }
        delete ui->listWidget_2->takeItem(ui->listWidget_2->row(items[0]));
        if(ui->listWidget_2->selectedItems().isEmpty())
        {
            ui->add_button_2->setEnabled(false);
            ui->remove_button_2->setEnabled(false);
            ui->selected_2->setText("");
        }
    }
}
void MainWindow::onLogout()
{
 user_.id=0;
 user_.fullname="";
 user_.hash_password="";
 user_.username="";
 ui->label_2->setText("");
 ui->selected->setText("");
 ui->label->clear();
 ui->selected_2->setText("");
 ui->remove_button->setEnabled(false);
 ui->edit_button->setEnabled(false);
 ui->remove_button_2->setEnabled(false);
 ui->add_button_2->setEnabled(false);
 ui->add_button->setEnabled(false);
 ui->listWidget->clear();
 ui->listWidget_2->clear();
 ui->search->setEnabled(false);
 ui->next->setEnabled(false);
 ui->previous->setEnabled(false);
 this->enter();
}

void MainWindow::on_search_textChanged(const QString &arg1)
{
    vector<Cat> cats=storage_->search(ui->search->text(),user_.id,on_page,(page-1)*on_page);
    serc=ui->search->text();
    int coun=storage_->number(serc,user_.id);
    max_page=1;
    while(coun/on_page!=0)
    {
        max_page++;
        coun=coun/on_page;
    }
    ui->pages->setText("page:"+QString::number(page)+"/"+QString::number(max_page));
    ui->listWidget->clear();
    this->addCats(cats);
    ui->edit_button->setEnabled(false);
    ui->remove_button->setEnabled(false);
    ui->add_button_2->setEnabled(false);
    ui->remove_button_2->setEnabled(false);
    ui->label->clear();
    ui->selected->clear();
    ui->selected_2->clear();
}
void MainWindow::onImport()
{
   QString file=QFileDialog::getOpenFileName(this,"Dialog Caption","","CSV (*.csv)");
   if(!QFile(file).exists())
   {
        QMessageBox::warning(this,"Import","Incorrect way to file. Import is failed.");
   }
   else
   {
       vector<Cat> cats=loadCats(file.toStdString());
       for(int i=0;i<cats.size();i++)
       {
           cats[i].user_id=user_.id;
           QFile file(QString::fromStdString(cats[i].photo));
           file.copy(QDir::currentPath()+QString::fromStdString("/../../cats/"+cats[i].name+".jpg"));
           cats[i].photo="/../../cats/"+cats[i].name+".jpg";
           storage_->insertCat(cats[i]);

       }
       int coun=storage_->number(serc,user_.id);
       max_page=1;
       while(coun/on_page!=0)
       {
           max_page++;
           coun=coun/on_page;
       }
       ui->pages->setText("page:"+QString::number(page)+"/"+QString::number(max_page));
       ui->listWidget->clear();
       this->addCats(cats);
       ui->edit_button->setEnabled(false);
       ui->remove_button->setEnabled(false);
       ui->add_button_2->setEnabled(false);
       ui->remove_button_2->setEnabled(false);
       ui->label->clear();
       ui->selected->clear();
       ui->selected_2->clear();

       this->addCats(storage_->search(serc,user_.id,on_page,(page-1)*on_page));
   }
}
void MainWindow::onExport()
{
    QString file=QFileDialog::getOpenFileName(this,"Dialog Caption","","CSV (*.csv)");
    if(!QFile(file).exists())
    {
         QMessageBox::warning(this,"Import","Incorrect way to file. Export is failed.");
    }
    else
    {
         vector<Cat> cats=storage_->search(serc,user_.id,max_page*on_page,0);
         saveCats(cats,file.toStdString());
    }
}

void MainWindow::on_next_clicked()
{
    if(page<max_page)
    {
        page++;
        ui->listWidget->clear();
        vector<Cat> cats=storage_->search(serc,user_.id,on_page,on_page*(page-1));
        ui->pages->setText("page:"+QString::number(page)+"/"+QString::number(max_page));
        this->addCats(cats);
        ui->selected->setText("");
        ui->label->clear();
        ui->remove_button->setEnabled(false);
        ui->edit_button->setEnabled(false);
        ui->selected_2->setText("");
        ui->listWidget_2->clear();
        ui->label_2->setText("");
        ui->add_button_2->setEnabled(false);
        ui->remove_button_2->setEnabled(false);
    }
}

void MainWindow::on_previous_clicked()
{
    if(page>1)
    {
        page--;
        ui->listWidget->clear();
        vector<Cat> cats=storage_->search(serc,user_.id,on_page,on_page*(page-1));
        ui->pages->setText("page:"+QString::number(page)+"/"+QString::number(max_page));
        this->addCats(cats);
        ui->selected->setText("");
        ui->label->clear();
        ui->remove_button->setEnabled(false);
        ui->edit_button->setEnabled(false);
        ui->selected_2->setText("");
        ui->listWidget_2->clear();
        ui->label_2->setText("");
        ui->add_button_2->setEnabled(false);
        ui->remove_button_2->setEnabled(false);
    }
}
