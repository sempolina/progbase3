#include "adddialog2.h"
#include "ui_adddialog2.h"
#include <QDebug>
adddialog2::adddialog2(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::adddialog2)
{
    ui->setupUi(this);
}
Breed adddialog2::data()
{
    QList <QListWidgetItem*> items=ui->listWidget->selectedItems();
    Breed al;
    foreach(QListWidgetItem *item,items)
    {
        al=item->data(Qt::UserRole).value<Breed>();
   }
  return al;
}
void adddialog2::setData(vector<Breed> al)
{
    for(const Breed& album:al)
    {
        QVariant qVar=QVariant::fromValue(album);
        QListWidgetItem * qAlbumListItem=new QListWidgetItem();
        qAlbumListItem->setText(QString::fromStdString(album.name));
        qAlbumListItem->setData(Qt::UserRole,qVar);
        ui->listWidget->addItem(qAlbumListItem);
    }
}
adddialog2::~adddialog2()
{
    delete ui;
}

void adddialog2::on_listWidget_itemClicked(QListWidgetItem *item)
{
  ui->buttonBox->setEnabled(true);
}
