CONFIG +=c++1z
#-------------------------------------------------
#
# Project created by QtCreator 2020-05-25T18:49:55
#
#-------------------------------------------------

QT       += core gui
QT += sql xml network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = client
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    adddialog.cpp \
    adddialog2.cpp \
    auth.cpp \
    choose.cpp \
    edditdialog.cpp \
    register.cpp \
    imp_exp.cpp \
    storage.cpp \
    sqlite_storage.cpp

HEADERS += \
        mainwindow.h \
    adddialog.h \
    adddialog2.h \
    auth.h \
    breed.h \
    cat.h \
    choose.h \
    edditdialog.h \
    register.h \
    user.h \
    imp_exp.h \
    storage.h \
    sqlite_storage.h

FORMS += \
        mainwindow.ui \
    adddialog.ui \
    adddialog2.ui \
    auth.ui \
    choose.ui \
    edditdialog.ui \
    mainwindow.ui \
    register.ui

SUBDIRS += \
    client.pro

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../csvlab/release/ -lcsvlab
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../csvlab/debug/ -lcsvlab
else:unix: LIBS += -L$$OUT_PWD/../csvlab/ -lcsvlab

INCLUDEPATH += $$PWD/../csvlab
DEPENDPATH += $$PWD/../csvlab

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../APIclient/release/ -lAPIclient
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../APIclient/debug/ -lAPIclient
else:unix: LIBS += -L$$OUT_PWD/../APIclient/ -lAPIclient

INCLUDEPATH += $$PWD/../APIclient
DEPENDPATH += $$PWD/../APIclient
