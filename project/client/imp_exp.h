#ifndef IMP_EXP_H
#define IMP_EXP_H
#include "csv.h"
#include "cat.h"
#include <fstream>
#include<vector>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <sstream>
#include <iomanip>
#include<QDir>

using namespace std;

vector <Cat> createCatListFromTable(StringTable &csvTable);
vector<Cat> loadCats(const string &file);
StringTable createTableFromCatList(vector<Cat> &ls);
void saveCats(const vector<Cat> &cats, const string &file);

#endif // IMP_EXP_H
