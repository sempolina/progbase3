#include "adddialog.h"
#include "ui_adddialog.h"
#include <QFile>
#include <QFileDialog>
using namespace std;
AddDialog::AddDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddDialog)
{
    ui->setupUi(this);
}

Cat AddDialog::data()
{
    Cat cat;
    cat.name =ui->lineEdit->text().toStdString();
    cat.age=ui->spinBox->value();
    cat.weight=ui->doubleSpinBox->value();
    QFile file(ui->lineEdit_2->text());
    file.copy(QDir::currentPath()+QString::fromStdString("/../../cats/"+cat.name+".jpg"));
    cat.photo="/../../cats/"+cat.name+".jpg";
    return cat;
}
AddDialog::~AddDialog()
{
    delete ui;
}

void AddDialog::on_pushButton_clicked()
{
    ui->lineEdit_2->setText(QFileDialog::getOpenFileName(this,"Dialog Caption","","JPEG (*.jpg)"));
}
