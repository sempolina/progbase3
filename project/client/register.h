#ifndef REGISTER_H
#define REGISTER_H
#include "user.h"
#include <QDialog>

namespace Ui {
class Register;
}

class Register : public QDialog
{
    Q_OBJECT

public:
    explicit Register(QWidget *parent = 0);
    User data();
    ~Register();

private slots:
    void on_lineEdit_5_textChanged(const QString &arg1);

private:
    Ui::Register *ui;
};

#endif // REGISTER_H
