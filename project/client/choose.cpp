#include "choose.h"
#include "ui_choose.h"

Choose::Choose(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Choose)
{
    ui->setupUi(this);
    connect(this,&Choose::signalFromButton,this,&Choose::set);
}

Choose::~Choose()
{
    delete ui;
}
bool Choose::data()
{
    return islogin_;

}
void Choose::on_pushButton_clicked()
{
    emit signalFromButton(false);
}

void Choose::on_pushButton_2_clicked()
{
    emit signalFromButton(true);
}
void Choose::set(bool islogin)
{
    islogin_=islogin;
    accept();
}
