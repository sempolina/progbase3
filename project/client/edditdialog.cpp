#include "edditdialog.h"
#include "ui_edditdialog.h"
#include <QFile>
#include <QFileDialog>
#include<QDebug>
EdditDialog::EdditDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EdditDialog)
{
    ui->setupUi(this);
}
void EdditDialog::setData(const Cat & cat)
{
    ui->lineEdit->setText(QString::fromStdString( cat.name));
    ui->spinBox->setValue(cat.age);
    ui->doubleSpinBox->setValue(cat.weight);
    ui->lineEdit_2->setText(QDir::currentPath()+QString::fromStdString(cat.photo));
}
Cat EdditDialog::data()
{
    Cat cat;
    cat.name=ui->lineEdit->text().toStdString();
    cat.age=ui->spinBox->value();
    cat.weight=ui->doubleSpinBox->value();
    QFile file(ui->lineEdit_2->text());
    if(!QFile(QDir::currentPath()+QString::fromStdString("/../../cats/"+cat.name+".jpg")).exists()||ui->lineEdit_2->text()==(QDir::currentPath()+QString::fromStdString("/../../cats/"+cat.name+".jpg")))
          file.copy(QDir::currentPath()+QString::fromStdString("/../../cats/"+cat.name+".jpg"));
    else if(file.exists())
    {
        QFile file2(QDir::currentPath()+QString::fromStdString("/../../cats/"+cat.name+".jpg"));
        file2.remove();
        file.copy(QDir::currentPath()+QString::fromStdString("/../../cats/"+cat.name+".jpg"));
    }
    cat.photo="/../../cats/"+cat.name+".jpg";
    return cat;
}
EdditDialog::~EdditDialog()
{
    delete ui;
}

void EdditDialog::on_pushButton_clicked()
{
    ui->lineEdit_2->setText(QFileDialog::getOpenFileName(this,"Dialog Caption","","JPEG (*.jpg)"));
}
