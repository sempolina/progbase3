#include "imp_exp.h"

using namespace std;

vector <Cat> createCatListFromTable(StringTable &csvTable)
{
    vector<Cat> ls;
    int i=0;
    if(csvTable.at(0, 0)=="name")
        i=1;
    for (; i < csvTable.size_rows(); i++)
    {
        Cat cat;
        cat.name=csvTable.at(i, 0);
        cat.age=atoi(csvTable.at(i, 1).c_str());
        cat.weight=atof(csvTable.at(i, 2).c_str());
        cat.photo=csvTable.at(i, 3);
        ls.push_back(cat);
    }
    return ls;
}
vector<Cat> loadCats(const string &file)
{
    ifstream fin;
    fin.open(file);
    if (!fin.is_open())
    {
        cout<<"File can`t be open"<<endl;
    }
    string str2;
    string str;
    getline(fin, str2);
    while (!fin.eof())
    {
        str.append(str2);
        str.push_back('\n');
        getline(fin, str2);
    }
    str.append(str2);
    str.push_back('\n');
    fin.close();
    StringTable inCsvTable = Csv_parse(str);
    vector<Cat> items = createCatListFromTable(inCsvTable);
    return items;
}
StringTable createTableFromCatList(vector<Cat> &ls)
{
    StringTable st{ls.size() + 1, 4};
    st.at(0, 0) = "name";
    st.at(0, 1) = "age";
    st.at(0,2)="weight";
    st.at(0,3) = "photo";
    for (int i = 0; i < ls.size(); i++)
    {
        st.at(i + 1, 0) = ls[i].name;
        st.at(i + 1, 1) = to_string(ls[i].age);
        stringstream stream;
        stream<<fixed<<setprecision(1)<<ls[i].weight;
        st.at(i + 1, 2) = stream.str();
        st.at(i + 1, 3) = QDir::currentPath().toStdString()+ls[i].photo;
    }
    return st;
}
void saveCats(const vector<Cat> &cats, const string &file)
{
    vector<Cat> cat = cats;
    StringTable outCsvTable = createTableFromCatList(cat);
    string strout = Csv_toString(outCsvTable);
    ofstream fout;
    fout.open(file);
    if (!fout.is_open())
    {
        cout<<"File can`t be open"<<endl;
    }
    fout<< strout;
    fout.close();
}

