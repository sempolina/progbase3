#include "sqlite_storage.h"
#include <QDebug>
#include <QCryptographicHash>
using namespace std;
SqliteStorage::SqliteStorage(const string &dir_name): Storage(dir_name)
{
db =QSqlDatabase ::addDatabase("QSQLITE");
}
bool SqliteStorage::isOpen() const
{
    return db.isOpen();
}
bool SqliteStorage::open()
{
   QString path =QString::fromStdString(this->name());
    db.setDatabaseName(path);
    if(!db.open())
        return false;
     return true;
}
void SqliteStorage::close()
{
    db.close();
}
vector<Cat> SqliteStorage::getAllCats()
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
    vector<Cat> cats;
    QSqlQuery query("SELECT * FROM cats");
    if(!query.exec())
    {
        qDebug()<<"get Cats error"<<query.lastError();
        this->close();
        return cats;
    }
       while(query.next())
       {
           int id=query.value("id").toInt();
           string name=query.value("name").toString().toStdString();
           int age=query.value("age").toInt();
           float weight=query.value("weight").toFloat();
           int user_id=query.value("user_id").toInt();
           string photo=query.value("photo").toString().toStdString();
           Cat cat;
           cat.id=id;
           cat.name=name;
           cat.age=age;
           cat.weight=weight;
           cat.photo=photo;
           cat.user_id=user_id;
           cats.push_back(cat);
       }
       this->close();
       return cats;
}
optional<Cat> SqliteStorage::getCatById(int cat_id)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("SELECT * FROM cats WHERE id = :id");
   query.bindValue(":id", cat_id);
   if(!query.exec())
   {
           qDebug()<<"get Cat error"<<query.lastError();
           this->close();
           return nullopt;
   }
   if(!query.next())
   {
       this->close();
       return nullopt;
   }
   int id=query.value("id").toInt();
   string name=query.value("name").toString().toStdString();
   int age=query.value("age").toInt();
   float weight=query.value("weight").toFloat();
   int user_id=query.value("user_id").toInt();
   string photo=query.value("photo").toString().toStdString();
   Cat cat;
   cat.id=id;
   cat.name=name;
   cat.age=age;
   cat.weight=weight;
   cat.photo=photo;
   cat.user_id=user_id;
   this->close();
   return cat;

}
bool SqliteStorage::updateCat(const Cat & cat)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("UPDATE cats SET name =:name, age =:age, weight =:weight, photo=:photo,user_id=:user_id WHERE id =:id;");
   query.bindValue(":id", cat.id);
   query.bindValue(":name", QString::fromStdString(cat.name));
   query.bindValue(":age", cat.age);
   query.bindValue(":weight", cat.weight);
   query.bindValue(":photo", QString::fromStdString(cat.photo));
   query.bindValue(":user_id",cat.user_id);
   if(!query.exec())
   {
           qDebug()<<"update photo error"<<query.lastError();
           this->close();
           return false;
   }
   if(!query.next())
   {
       this->close();
       return false;
   }
   this->close();
   return true;
}
bool SqliteStorage::removeCat(int cat_id)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
 QSqlQuery query;
 query.prepare("DELETE FROM cats WHERE id = :id");
 query.bindValue(":id", cat_id);
 if(!query.exec())
 {
         qDebug()<<"delete Cat error"<<query.lastError();
         this->close();
         return false;
 }
 if(!query.numRowsAffected())
 {
     this->close();
     return false;
 }
 this->close();
 if(!this->open())
 {
     qDebug()<<"Database can`t be open";
     exit(1);
 }
 QSqlQuery query2;
 query2.prepare("DELETE FROM links WHERE id_cat = :id");
 query2.bindValue(":id", cat_id);
 if(!query2.exec())
 {
         qDebug()<<"delete link error"<<query2.lastError();
         this->close();
         return false;
 }
 this->close();
 return true;
}
int SqliteStorage::insertCat(const Cat & cat)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("INSERT INTO cats (name, age, weight, photo, user_id) VALUES (:name, :age, :weight, :photo, :user_id)");
   query.bindValue(":name", QString::fromStdString(cat.name));
   query.bindValue(":age", cat.age);
   query.bindValue(":weight", cat.weight);
   query.bindValue(":photo", QString::fromStdString(cat.photo));
   query.bindValue(":user_id",cat.user_id);
   if(!query.exec())
   {
       qDebug()<<"add Cat error"<<query.lastError();
       this->close();
       return 0;
   }
   int id=query.lastInsertId().toInt();
   this->close();
   return id;
}
vector<Breed> SqliteStorage::getAllBreeds()
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
    vector<Breed> breeds;
    QSqlQuery query("SELECT * FROM breeds");
    if(!query.exec())
    {
        qDebug()<<"get Breeds error"<<query.lastError();
        this->close();
        return  breeds;
    }
       while(query.next())
       {
           int id=query.value("id").toInt();
           string name=query.value("name").toString().toStdString();
           int age_of_life=query.value("age_of_life").toInt();
           string size=query.value("size").toString().toStdString();
           Breed breed;
           breed.id=id;
           breed.name=name;
           breed.age_of_life=age_of_life;
           breed.size=size;
           breeds.push_back(breed);
       }
       this->close();
       return breeds;
}
optional<Breed> SqliteStorage::getBreedById(int breed_id)
{
    
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("SELECT * FROM breeds WHERE id = :id");
   query.bindValue(":id", breed_id);
   if(!query.exec())
   {
           qDebug()<<"get Breed error"<<query.lastError();
           this->close();
           return nullopt;
   }
   if(!query.next())
   {
       this->close();
       return nullopt;
   }
   int id=query.value("id").toInt();
   string name=query.value("name").toString().toStdString();
   int age_of_life=query.value("age_of_life").toInt();
   string size=query.value("size").toString().toStdString();
   Breed breed;
   breed.id=id;
   breed.name=name;
   breed.age_of_life=age_of_life;
   breed.size=size;
   this->close();
   return breed;
}
bool SqliteStorage::updateBreed(const Breed & breed)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("UPDATE breeds SET name = :name, size= :size, age_of_life = :age_of_life WHERE id = :id");
   query.bindValue(":id", breed.id);
   query.bindValue(":name", QString::fromStdString(breed.name));
   query.bindValue(":size", QString::fromStdString(breed.size));
   query.bindValue(":age_of_life", breed.age_of_life);
   if(!query.exec())
   {
           qDebug()<<"update Breed error"<<query.lastError();
           this->close();
           return false;
   }
   if(!query.next())
   {
       this->close();
       return false;
   }
   this->close();
   return true;
}
bool SqliteStorage::removeBreed(int breed_id)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
 QSqlQuery query;
 query.prepare("DELETE FROM breeds WHERE id = :id");
 query.bindValue(":id",breed_id);
 if(!query.exec())
 {
         qDebug()<<"delete Breed error"<<query.lastError();
         this->close();
         return false;
 }
 if(!query.numRowsAffected())
 {
     this->close();
     return false;
 }
 this->close();
 return true;
    
}
int SqliteStorage::insertBreed(const Breed & breed)
{
    if(!this->open())
    {
        qDebug()<<"Database can`t be open";
        exit(1);
    }
   QSqlQuery query;
   query.prepare("INSERT INTO breeds (name,size, age_of_life) VALUES (:name,:size,:age_of_life)");
   query.bindValue(":name", QString::fromStdString(breed.name));
   query.bindValue(":size", QString::fromStdString(breed.size));
   query.bindValue(":age_of_life", breed.age_of_life);
   if(!query.exec())
   {
       qDebug()<<"add Breed error"<<query.lastError();
       this->close();
       return 0;
   }
   int id=query.lastInsertId().toInt();
   this->close();
   return id;
}
static QString hashPassword(QString const & pass)
{
    QByteArray pass_ba=pass.toUtf8();
    pass_ba=QCryptographicHash::hash(pass_ba,QCryptographicHash::Md5);
    QString pass_hash=QString(pass_ba.toHex());
    return pass_hash;
}
optional<User> SqliteStorage::getUserAuth( QString const & username, QString const & password)
{
    qDebug()<<"Hello";
    if(!this->open())
    {

        throw "Database can`t be open";
     }
  QSqlQuery query;
  if(!query.prepare("SELECT * FROM users WHERE username=:username AND password_hash=:password_hash;"))
  {
      QSqlError error=query.lastError();
      throw error;
  }
  query.bindValue(":username",username);
  query.bindValue(":password_hash",hashPassword(password));
  if(!query.exec())
  {
     QSqlError error =query.lastError();
     throw error;
  }
  if(query.next())
  {
      User user;
      user.id =query.value("id").toInt();
      user.username=username;
      user.hash_password=query.value("password_hash").toString();
      user.fullname=query.value("fullname").toString();
      this->close();
      return user;
  }
  this->close();
  return nullopt;
}
vector<Cat> SqliteStorage::getAllUserCats(int user_id)
{
    if(!this->open())
    {
        throw "Database can`t be open";
    }
    QSqlQuery query;
    if(!query.prepare("SELECT * FROM cats WHERE user_id=:user_id"))
    {
        QSqlError error=query.lastError();
        throw error;
    }
    query.bindValue(":user_id",user_id);
    if(!query.exec())
    {
       QSqlError error =query.lastError();
       throw error;
    }
    vector<Cat> cats;
    while(query.next())
    {
        Cat c;
        c.id=query.value(0).toInt();
        c.name=query.value(1).toString().toStdString();
        c.age=query.value(2).toInt();
        c.weight=query.value(3).toFloat();
        c.photo=query.value(4).toString().toStdString();
        c.user_id=query.value(5).toInt();
        qDebug()<<c.user_id;
        cats.push_back(c);
    }
    this->close();
    return cats;

}
int SqliteStorage::registerUser(QString const & username,QString const & password, QString const & fullname)
{
    if(!this->open())
    {
        throw "Database can`t be open";
    }
    QSqlQuery query;
    query.prepare("SELECT * FROM users WHERE username=:username");
    query.bindValue(":username",username);
    if(!query.exec())
    {
       QSqlError error =query.lastError();
       throw error;
    }
    if(!query.next())
    {
        if(!query.prepare("INSERT INTO users (username, password_hash, fullname) VALUES(:username, :password_hash, :fullname)"))
        {
            QSqlError error =query.lastError();
            throw error;
        }
        query.bindValue(":username",username);
        query.bindValue(":password_hash",hashPassword(password));
        query.bindValue(":fullname",fullname);
        if(!query.exec())
        {
           QSqlError error =query.lastError();
           throw error;
        }
        QVariant id=query.lastInsertId();
        this->close();
        return  id.toInt();
    }
    else
    {
        this->close();
        return -1;
    }
}
vector<Breed> SqliteStorage::getAllCatBreeds(int cat_id)
{
    if(!this->open())
    {
        throw "Database can`t be open";
    }
    QSqlQuery query;
    if(!query.prepare("SELECT breeds.id,breeds.name,breeds.size,breeds.age_of_life FROM cats INNER JOIN links ON links.id_cat=cats.id "
                      "INNER JOIN breeds ON links.id_breed=breeds.id AND cats.id=:cat_id"))
        {
            QSqlError error=query.lastError();
            throw error;
        }
     query.bindValue(":cat_id",cat_id);
        if(!query.exec())
        {
           QSqlError error =query.lastError();
           throw error;
        }
    vector<Breed> breeds;
    while(query.next())
    {
                    Breed b;
                    b.id=query.value("id").toInt();
                    b.name=query.value("name").toString().toStdString();
                    b.size=query.value("size").toString().toStdString();
                    b.age_of_life=query.value("age_of_life").toInt();
                    breeds.push_back(b);
    }
    this->close();
    return breeds;
}
bool SqliteStorage::insertCatBreeds(int cat_id,int breed_id)
{
    if(!this->open())
    {
        throw "Database can`t be open";
    }
    QSqlQuery query;
    query.prepare("INSERT INTO links (id_cat,id_breed) VALUES (:id_cat,:id_breed);");
    query.bindValue(":id_cat",cat_id);
    query.bindValue(":id_breed", breed_id);
    if(!query.exec())
    {
        QSqlError error =query.lastError();
        throw error;
        this->close();
        return false;
    }
    this->close();
    return true;
}
bool SqliteStorage::removeCatBreed(int cat_id,int breed_id)
{
    if(!this->open())
    {
        throw "Database can`t be open";
    }
    QSqlQuery query;
    query.prepare("DELETE FROM links WHERE id_cat=:id_cat AND id_breeds=:id_breed;");
    query.bindValue(":id_cat",cat_id);
    query.bindValue(":id_breed", breed_id);
    if(!query.exec())
    {
        QSqlError error =query.lastError();
        throw error;
        this->close();
        return false;
    }
    this->close();
    return true;
}
vector<Cat> SqliteStorage::search(QString const & s,int use_id,int number,int miss)
{
    if(!this->open())
    {
        throw "Database can`t be open";
    }
    QSqlQuery query;
    query.prepare("SELECT * FROM cats WHERE name LIKE ('%'|| :s || '%') AND user_id=:user_id LIMIT :number OFFSET :miss;");
    query.bindValue(":s",s);
    query.bindValue(":user_id",use_id);
    query.bindValue(":number",number);
    query.bindValue(":miss",miss);
    vector<Cat> cats;
    if(!query.exec())
    {
        QSqlError error =query.lastError();
        throw error;
        this->close();
        return cats;
    }
    while(query.next())
    {
        int id=query.value("id").toInt();
        string name=query.value("name").toString().toStdString();
        int age=query.value("age").toInt();
        float weight=query.value("weight").toFloat();
        int user_id=query.value("user_id").toInt();
        string photo=query.value("photo").toString().toStdString();
        Cat cat;
        cat.id=id;
        cat.name=name;
        cat.age=age;
        cat.weight=weight;
        cat.photo=photo;
        cat.user_id=user_id;
        cats.push_back(cat);
    }
    this->close();
    return cats;
}
int SqliteStorage::number(QString const & s,int use_id)
{
    if(!this->open())
    {
        throw "Database can`t be open";
    }
    QSqlQuery query;
    query.prepare("SELECT COUNT(*) FROM cats WHERE name LIKE ('%'|| :s || '%') AND user_id=:user_id");
    query.bindValue(":s",s);
    query.bindValue(":user_id",use_id);
    if(!query.exec())
    {
        QSqlError error =query.lastError();
        throw error;
        this->close();
        return 0;
    }
    int n;
    if(query.next())
    {
        n=query.value("COUNT(*)").toInt();
    }
    this->close();
    return n;
}
