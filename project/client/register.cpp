#include "register.h"
#include "ui_register.h"

Register::Register(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Register)
{
    ui->setupUi(this);
    ui->lineEdit_4->setEchoMode(QLineEdit::Password);
    ui->lineEdit_5->setEchoMode(QLineEdit::Password);
}

Register::~Register()
{
    delete ui;
}
User Register::data()
{
    User user;
    user.fullname=ui->lineEdit->text();
    user.fullname.append(" ");
    user.fullname.append(ui->lineEdit_2->text());
    user.username=ui->lineEdit_3->text();
    user.hash_password=ui->lineEdit_4->text();
    return user;
}
void Register::on_lineEdit_5_textChanged(const QString &arg1)
{
    if ((ui->lineEdit_5->text()==ui->lineEdit_4->text())&&(ui->lineEdit_5->text().isEmpty()==false)&&(ui->lineEdit->text()!="")&&(ui->lineEdit_2->text()!="")&&(ui->lineEdit_3->text()!=""))
    {
        ui->buttonBox->setEnabled(true);
    }
    else
    {
        ui->buttonBox->setEnabled(false);
    }
}
