#pragma once
#ifndef PHOTO_H
#define PHOTO_H
#include<string>
#include <iostream>
#include <QListWidgetItem>
using namespace std;
struct Cat
{
    int id;
    string name;
    int age;
    float weight;
    string photo;
    int user_id;
    Cat(){}
    Cat(const Cat & al){this->id=al.id;this->name=al.name;this->age =al.age;this->weight=al.weight;this->photo=al.photo;this->user_id=al.user_id;}
};
Q_DECLARE_METATYPE(Cat)

#endif // PHOTO_H
