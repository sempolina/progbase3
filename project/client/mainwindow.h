#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "sqlite_storage.h"
#include"user.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    User user_;
    QString serc="";
    int page=1;
    int on_page=11;
    int max_page=0;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void addCats(const vector<Cat> & cats);
    void addBreeds(const vector<Breed> & breeds);
    void enter();
private slots:
    void onOpen();

    void onLogout();
    void on_listWidget_itemClicked(QListWidgetItem *item);
    void on_remove_button_clicked();

    void on_add_button_clicked();

    void on_edit_button_clicked();
    void beforeClose();
    void on_listWidget_2_itemClicked(QListWidgetItem *item);

    void on_add_button_2_clicked();

    void on_remove_button_2_clicked();

    void on_search_textChanged(const QString &arg1);
    void onImport();
    void onExport();
    void on_next_clicked();

    void on_previous_clicked();

private:
    Ui::MainWindow *ui;
    Storage * storage_=nullptr;
};

#endif // MAINWINDOW_H
