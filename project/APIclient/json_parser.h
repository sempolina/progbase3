#ifndef JSON_PARSER_H
#define JSON_PARSER_H

#include "cat2.h"
#include "breed2.h"
#include "user2.h"
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <vector>
#include <QString>
using namespace std;
vector<Cat> jsonToCats(const QString &str);
vector<Breed> jsonToBreeds(const QString &str);
QString catstojson(vector<Cat> cats);
QString breedstojson(vector<Breed> breeds);
User jsonTouser(const QString &str);
QString userstojson(User user);
#endif // JSON_PARSER_H
