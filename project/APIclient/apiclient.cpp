#include "apiclient.h"

using namespace std;
APIclient::APIclient(const QString & server_url):server_url_{server_url}
{

}
static void httpGet(QNetworkAccessManager & manager,const QString & url,function<void (NetworkException *error,QString * data)>handler,const QString &username="",const QString &password="")
{
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    if((username!="")&&(password!=""))
    {
        QString concatenated=username+":"+password;
        QByteArray data=concatenated.toUtf8().toBase64();
        QString headerData ="Basic "+data;
        request.setRawHeader("Authorization",headerData.toUtf8());
    }
    QNetworkReply *reply=manager.get(request);
    QObject::connect(reply,&QNetworkReply::finished,[=]{
        qDebug()<<"# got reply";
        QString replyBodyString =QString::fromUtf8(reply->readAll());
        QNetworkReply::NetworkError error =reply->error();
        reply->deleteLater();
            if(error!=QNetworkReply::NetworkError::NoError)
            {
                NetworkException ex(error,replyBodyString);
                handler(&ex,nullptr);
            }
            else
            {
                handler(nullptr,&replyBodyString);
            }
    }

    );
}
static void httpPost(QNetworkAccessManager & manager,const QString & url,const QString & content_type, const QString & body,function<void (NetworkException *error,QString * data)>handler,const QString &username="",const QString &password="")
{
    QNetworkRequest request;
    request.setUrl(QUrl(url));
    request.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader,content_type);
    if((username!="")&&(password!=""))
    {
        QString concatenated=username+":"+password;
        QByteArray data=concatenated.toUtf8().toBase64();
        QString headerData ="Basic "+data;
        request.setRawHeader("Authorization",headerData.toUtf8());
    }
    QNetworkReply *reply=manager.post(request,body.toUtf8());
    QObject::connect(reply,&QNetworkReply::finished,[=]{
        qDebug()<<"# got reply";
        QString replyBodyString =QString::fromUtf8(reply->readAll());
        QNetworkReply::NetworkError error =reply->error();
        reply->deleteLater();
            if(error!=QNetworkReply::NetworkError::NoError)
            {
                NetworkException ex(error,replyBodyString);
                handler(&ex,nullptr);
            }
            else
            {
                handler(nullptr,&replyBodyString);
            }
    }

    );
}
static QString httpPut(QNetworkAccessManager & manager,const QString & url,const QString & content_type, const QString & body)
{
    QNetworkRequest request{url};
    request.setHeader(QNetworkRequest::KnownHeaders::ContentTypeHeader,content_type);
    NetworkWaiter waiter{manager};
    QNetworkReply *reply=manager.put(request,body.toUtf8());
    waiter.wait();
    QString replyBodyString =QString::fromUtf8(reply->readAll());
    QNetworkReply::NetworkError error =reply->error();
    reply->deleteLater();
    if(error!=QNetworkReply::NetworkError::NoError)
        throw NetworkException(error,replyBodyString);
    return replyBodyString;
}
static QString httpDelete(QNetworkAccessManager & manager,const QString & url)
{
    QNetworkRequest request{url};
    NetworkWaiter waiter{manager};
    QNetworkReply *reply=manager.deleteResource(request);
    waiter.wait();
    QString replyBodyString =QString::fromUtf8(reply->readAll());
    QNetworkReply::NetworkError error =reply->error();
    reply->deleteLater();
    if(error!=QNetworkReply::NetworkError::NoError)
        throw NetworkException(error,replyBodyString);
    return replyBodyString;
}
bool APIclient::updateCat(const Cat & cat)
{
    QString url=server_url_+"/cats";
    vector<Cat> cats;
    cats.push_back(cat);
    QString body = catstojson(cats);
    try{
    httpPut(manager,url,"text/json",body);
    return true;
    }
    catch(const NetworkException & nex)
    {
        return false;
    }
}
bool APIclient::removeCat(int cat_id)
{
    try
  {
     QString url =server_url_+"/cats/"+QString::number(cat_id);
     httpDelete(manager,url);
     return true;
   }
    catch(const NetworkException & nex)
    {
        return false;
    }
}
int APIclient:: insertCat(const Cat & cat)
{
   QString url=server_url_+"/cats";
   vector<Cat> cats;
   cats.push_back(cat);
   QString body = catstojson(cats);
   httpPost(manager,url,"text/json",body,[&](NetworkException * error,QString * data)
   {
      if(error!=nullptr)
      {
          qDebug()<<"# got an error: "<<error->message;
          return -1;
      }
      else
      {
          qDebug()<<"# got result:"<<*data;
          return jsonToCats(*data)[0].id;
      }
   }
   );
}
vector<Breed> APIclient::getAllBreeds()
{
    QString url=server_url_+"/breeds";
    httpGet(manager,url,[&](NetworkException * error,QString * data)
        {
           if(error!=nullptr)
           {
               qDebug()<<"# got an error: "<<error->message;
               vector<Breed> breeds;
               return breeds;
           }
           else
           {
               qDebug()<<"# got result:"<<*data;
               return jsonToBreeds(*data);
           }
        });
}
int APIclient::registerUser(QString const & username,QString const & password, QString const & fullname)
{
    QString url=server_url_+"/user" ;
    try{
          User user;
          user.id=0;
          user.username=username;
          user.hash_password=password;
          user.fullname=fullname;
          QString body = userstojson(user);
          httpPost(manager,url,"text/json",body,[&](NetworkException * error,QString * data)
          {
             if(error!=nullptr)
             {
                 qDebug()<<"# got an error: "<<error->message;
                 return jsonTouser(*data).id;
             }
             else
             {
                 qDebug()<<"# got result:"<<*data;
                 return -1;
             }
          },username,password
          );
    }
    catch(const NetworkException & nex)
    {
        return -1;
    }
}
optional<User> APIclient::getUserAuth(QString const & username, QString const & password)
{
    QString url=server_url_+"/user";
    httpGet(manager,url,[&](NetworkException * error,QString * data)
        {
           if(error!=nullptr)
           {
               qDebug()<<"# got an error: "<<error->message;
               return jsonTouser((*data));
           }
           else
           {
               qDebug()<<"# got result:"<<*data;
               return jsonTouser((*data));
           }
        },username,password);
}
vector<Breed> APIclient::getAllCatBreeds(int cat_id)
{
    QString url=server_url_+"/cats/"+QString::number(cat_id)+"/breeds";
    httpGet(manager,url,[&](NetworkException * error,QString * data)
        {
           if(error!=nullptr)
           {
               qDebug()<<"# got an error: "<<error->message;
               vector<Breed> cats;
               return cats;
           }
           else
           {
               qDebug()<<"# got result:"<<*data;
               return jsonToBreeds((*data));
           }
        });
}
bool APIclient::insertCatBreeds(int cat_id,int breed_id)
{
     QString url =server_url_+"/cats/"+QString::number(cat_id)+"/breeds/"+QString::number(breed_id);
     httpPost(manager,url,"text/json","",[&](NetworkException * error,QString * data)
     {
        if(error!=nullptr)
        {
            qDebug()<<"# got an error: "<<error->message;
            return false;
        }
        else
        {
            qDebug()<<"# got result:"<<*data;
            return true;
        }
     }
     );
}
bool APIclient::removeCatBreed(int cat_id,int breed_id)
{
    try
  {
     QString url =server_url_+"/cats/"+QString::number(cat_id)+"/breeds/"+QString::number(breed_id);
     httpDelete(manager,url);
     return true;
   }
    catch(const NetworkException & nex)
    {
        return false;
    }
}
vector<Cat> APIclient::search(QString const & s,int use_id,int number,int miss)
{
    QString url=server_url_+"/cats?search="+s+"&user_id="+QString::number(use_id)+"&number="+QString::number(number)+"&miss="+QString::number(miss);
    httpGet(manager,url,[&](NetworkException * error,QString * data)
        {
           if(error!=nullptr)
           {
               qDebug()<<"# got an error: "<<error->message;
               vector<Cat> cats;
               return cats;
           }
           else
           {
               qDebug()<<"# got result:"<<*data;
               return jsonToCats((*data));
           }
        });
}
int APIclient::number(QString const & s,int use_id)
{
    QString url=server_url_+"/cats/count?search="+s+"&user_id="+QString::number(use_id);
    httpGet(manager,url,[&](NetworkException * error,QString * data)
        {
           if(error!=nullptr)
           {
               qDebug()<<"# got an error: "<<error->message;
               return 0;
           }
           else
           {
               qDebug()<<"# got result:"<<*data;
               return data->toInt();
           }
        });
}
void APIclient::open(QString const & filename)
{
    qDebug()<<"Hello";
    QByteArray data=filename.toUtf8().toBase64();
    QString url =server_url_+"/database/"+data;
    httpPost(manager,url,"text/json","",[&](NetworkException * error,QString * data)
    {
       if(error!=nullptr)
       {
           qDebug()<<"# got an error: "<<error->message;
       }
       else
       {
           qDebug()<<"# got result:"<<*data;
       }
    }
    );
}
