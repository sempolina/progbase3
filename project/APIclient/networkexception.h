#ifndef NETWORKEXCEPTION_H
#define NETWORKEXCEPTION_H
#include <QException>
#include <QNetworkReply>
using namespace std;
class NetworkException : QException
{
public:
    const QNetworkReply::NetworkError error;
    const QString message;
    NetworkException(QNetworkReply::NetworkError error,const QString & mesage) :error{error},message{mesage}{}
};
#endif // NETWORKEXCEPTION_H

