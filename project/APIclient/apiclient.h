#ifndef APICLIENT_H
#define APICLIENT_H
#include <vector>
#include "cat2.h"
#include "user2.h"
#include "breed2.h"
#include "json_parser.h"
#include <QNetworkAccessManager>
#include "networkwaiter.h"
#include "networkexception.h"
#include <optional>

using namespace std;

class APIclient
{
    const QString server_url_;
    QNetworkAccessManager manager;

public:
    explicit APIclient(const QString & server_url);
    bool updateCat(const Cat & cat);
    bool removeCat(int cat_id);
    int  insertCat(const Cat & cat);
    vector<Breed> getAllBreeds();
    int registerUser(QString const & username,QString const & password, QString const & fullname);
    optional<User> getUserAuth(QString const & username, QString const & password);
    vector<Breed> getAllCatBreeds(int cat_id);
    bool insertCatBreeds(int cat_id,int breed_id);
    bool removeCatBreed(int cat_id,int breed_id);
    vector<Cat> search(QString const & s,int use_id,int number,int miss);
    int number(QString const & s,int use_id);
    void open(QString const & filename);
};

#endif // APICLIENT_H
