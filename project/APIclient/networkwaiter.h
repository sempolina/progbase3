#ifndef NETWORKWAITER_H
#define NETWORKWAITER_H
#include <QCoreApplication>
#include <QNetworkAccessManager>
using namespace std;
class NetworkWaiter
{
    int argc =0;
    QCoreApplication app;
public:
    explicit NetworkWaiter(QNetworkAccessManager & manager) :app (argc,nullptr)
    {
        QObject::connect(&manager,&QNetworkAccessManager::finished,&app,&QCoreApplication::quit);
    }
    void wait() {app.exec();}
};
#endif // NETWORKWAITER_H
