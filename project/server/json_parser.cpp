#include "json_parser.h"
using namespace std;

vector<Cat> jsonToCats(const QString &str)
{
     QJsonParseError err;
  QJsonDocument doc=QJsonDocument::fromJson(str.toUtf8(),&err);
  vector<Cat> cats;
  if(err.error!=QJsonParseError::NoError)
  {
      throw  err.error;
      return cats;
  }
  QJsonArray rootArray =doc.array();
  for(QJsonValue jvalue: rootArray)
  {
      QJsonObject jobj=jvalue.toObject();
      Cat cat;
      cat.id=jobj.value("id").toInt();
      cat.name=jobj.value("name").toString().toStdString();
      cat.age=jobj.value("age").toInt();
      cat.weight=jobj.value("weight").toDouble();
      cat.photo=jobj.value("photo").toString().toStdString();
      cat.user_id=jobj.value("user_id").toInt();
      cats.push_back(cat);
  }
  return cats;
}
vector<Breed> jsonToBreeds(const QString &str)
{
  QJsonParseError err;
  QJsonDocument doc=QJsonDocument::fromJson(str.toUtf8(),&err);
  vector<Breed> breeds;
  if(err.error!=QJsonParseError::NoError)
  {
      throw  err.error;
      return breeds;
  }
  QJsonArray rootArray =doc.array();
  for(QJsonValue jvalue: rootArray)
  {
      QJsonObject jobj=jvalue.toObject();
      Breed breed;
      breed.id=jobj.value("id").toInt();
      breed.age_of_life=jobj.value("age_of_life").toInt();
      breed.name=jobj.value("name").toString().toStdString();
      breed.size=jobj.value("size").toInt();
      breeds.push_back(breed);
  }
  return breeds;
}
QString catstojson(vector<Cat> cats)
{
    QJsonDocument doc;
    QJsonArray catArray;
    for(const Cat & cat: cats)
    {
         QJsonObject cObj;
         cObj.insert("id",cat.id);
         cObj.insert("name",QString::fromStdString(cat.name));
         cObj.insert("age",cat.age);
         cObj.insert("weight",cat.weight);
         cObj.insert("photo",QString::fromStdString(cat.photo));
         cObj.insert("user_id",cat.user_id);
         catArray.push_back(cObj);
    }
    doc.setArray(catArray);
    QString qStr =doc.toJson(QJsonDocument::JsonFormat::Indented);
    return qStr;
}
QString breedstojson(vector<Breed> breeds)
{
    QJsonDocument doc;
    QJsonArray breedArray;
    for(const Breed & breed: breeds)
    {
         QJsonObject cObj;
         cObj.insert("id",breed.id);
         cObj.insert("name",QString::fromStdString(breed.name));
         cObj.insert("size",QString::fromStdString(breed.size));
         cObj.insert("age_of_life",breed.age_of_life);
         breedArray.push_back(cObj);
    }
    doc.setArray(breedArray);
    QString qStr =doc.toJson(QJsonDocument::JsonFormat::Indented);
    return qStr;
}
User jsonTouser(const QString &str)
{
      QJsonParseError err;
  QJsonDocument doc=QJsonDocument::fromJson(str.toUtf8(),&err);
    User user;
  if(err.error!=QJsonParseError::NoError)
  {
      throw  err.error;
       return user;
  }
  QJsonObject jobj=doc.object();
      user.id=jobj.value("id").toInt();
      user.fullname=jobj.value("fullname").toString();
  return user;
}
QString userstojson(User user)
{
    QJsonDocument doc;
    QJsonObject cObj;
    cObj.insert("id",user.id);
    cObj.insert("fulname",user.fullname);
    doc.setObject(cObj);
    QString qStr =doc.toJson(QJsonDocument::JsonFormat::Indented);
    return qStr;
}

