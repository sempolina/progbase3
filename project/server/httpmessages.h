#ifndef HTTPMESSAGES_H
#define HTTPMESSAGES_H

# include <QMap>
#include <QTextStream>
#include <QUrlQuery>
#include<QString>
using namespace std;

struct HttpRequest
{
    QString method;
    QString uri;
    QString http_version;
    QMap <QString,QString> headers;
    QString body;
    QString user;
    QString password;
    QMap<QString,QString> params;
    QMap<QString,QString> query;
    QString contentType() const;
    int contentLenght() const;
};
struct HttpResponse
{
  QString http_version;
int status_code;
QString status_description;
QMap<QString,QString> headers;
QString body;

void setStatus(int code,const QString & description="");

void setContentType(const QString & type);
void setContentLenght(int lenght);
};

HttpRequest parseHttpRequest(const QString & str);
QMap<QString,QString> parseUrlQuery(const QString & uri);
QString formatHttpResponse(const HttpResponse & res);
bool patternMatch(const QString & pattern,const QString & path,QMap<QString,QString> & params);
#endif // HTTPMESSAGES_H
