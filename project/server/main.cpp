#include <QCoreApplication>
#include  "httpserver.h"
#include "apiserver.h"
using namespace std;
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    APIserver api;
    api.httpserver.get("/cats/:id/breeds" ,[&](HttpRequest & req,HttpResponse & res)
    {
        int id=req.params["id"].toInt();
        vector<Breed> breeds=api.storage_->getAllCatBreeds(id);
        QString str=breedstojson(breeds);
        res.setContentType("text/json");
        res.body=str;
    });
    api.httpserver.get("/cats" ,[&](HttpRequest & req,HttpResponse & res)
    {
        QString search =req.query["search"];
        QString user_id=req.query["user_id"];
        QString number=req.query["number"];
        QString miss=req.query["miss"];
        vector<Cat> cats=api.storage_->search(search,user_id.toInt(),number.toInt(),miss.toInt());
        QString str=catstojson(cats);
        res.setContentType("text/json");
        res.body=str;
    });
    api.httpserver.get("/cats/count" ,[&](HttpRequest & req,HttpResponse & res)
    {
        QString search =req.query["search"];
        QString user_id=req.query["user_id"];
        int count=api.storage_->number(search,user_id.toInt());
        res.setContentType("text/json");
        res.body=QString::number(count);
    });
    api.httpserver.get("/breeds" ,[&](HttpRequest & req,HttpResponse & res)
    {
        vector<Breed> breeds=api.storage_->getAllBreeds();
        QString str=breedstojson(breeds);
        res.setContentType("text/json");
        res.body=str;
    });
    api.httpserver.get("/user" ,[&](HttpRequest & req,HttpResponse & res)
    {
        if(api.storage_->getUserAuth(req.user,req.password)==nullopt)
        {
            res.setStatus(404,"Not Found");
        }
        else
        {
            User user=api.storage_->getUserAuth(req.user,req.password).value();
            QString str=userstojson(user);
            res.setContentType("text/json");
            res.body=str;
        }
    });
    api.httpserver.post("/cats" ,[&](HttpRequest & req,HttpResponse & res)
    {
        Cat new_cat = jsonToCats(req.body)[0];
        new_cat.id=api.storage_->insertCat(new_cat);
        res.setContentType("text/json");
        vector<Cat> cats;
        cats.push_back(new_cat);
        res.body=catstojson(cats);
     });
    api.httpserver.post("/user" ,[&](HttpRequest & req,HttpResponse & res)
    {
        User user = jsonTouser(req.body);
        user.id=api.storage_->registerUser(user.username,user.hash_password,user.fullname);
        if(user.id==-1)
        {
            res.setStatus(404,"Not Found");
        }
        else
        {
            res.setContentType("text/json");
            res.body=userstojson(user);
        }
     });
    api.httpserver.post("/cats/:id/breeds/:breed_id" ,[&](HttpRequest & req,HttpResponse & res)
    {
        int id=req.params["id"].toInt();
        int breed_id=req.params["id_breed"].toInt();
        if(!api.storage_->insertCatBreeds(id,breed_id))
        {
            res.setStatus(404,"Not Found");
        }
     });
    api.httpserver.put("/cats" ,[&](HttpRequest & req,HttpResponse & res)
    {
        Cat update_cat=jsonToCats(req.body)[0];
        if(api.storage_->updateCat(update_cat))
        {
            res.setContentType("text/json");
            vector<Cat> cats;
            cats.push_back(update_cat);
            res.body=catstojson(cats);
        }
        else
        {
            res.setStatus(404,"Not Found");
        }
    });
    api.httpserver.delete_("/cats/:id" ,[&](HttpRequest & req,HttpResponse & res)
    {
        int delete_id=req.params["id"].toInt();
        if(!api.storage_->removeCat(delete_id))
        {
            res.setStatus(404,"Not Found");
        }
    });
    api.httpserver.delete_("/cats/:id/breeds/:id_breed" ,[&](HttpRequest & req,HttpResponse & res)
    {
        int id=req.params["id"].toInt();
        int breed_id=req.params["id_breed"].toInt();
        if(!api.storage_->removeCatBreed(id,breed_id))
        {
            res.setStatus(404,"Not Found");
        }
    });
    api.httpserver.post("/database/:name" ,[&](HttpRequest & req,HttpResponse & res)
    {
        QByteArray ba;
        ba.append(req.query["name"]);
        QString name=QByteArray::fromBase64(ba);
        qDebug()<<name;
        if(api.storage_!=nullptr)
        {
            delete api.storage_;
        }
        api.storage_=new SqliteStorage {name.toStdString()};
    });

    const int port =3000;
    if(api.httpserver.start(port))
        qDebug()<<" started at port"<<port;
     else
        qDebug()<<"can`t start at port"<<port;
    return a.exec();
}
