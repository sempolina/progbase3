#ifndef APISERVER_H
#define APISERVER_H
#include "httpserver.h"
#include "sqlite_storage.h"

using namespace std;

class APIserver
{
public:
    APIserver();
    ~APIserver();
    Storage *storage_=nullptr;
    HttpServer httpserver;
};

#endif // APISERVER_H
