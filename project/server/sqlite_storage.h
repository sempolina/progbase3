#ifndef SQLITE_STORAGE_H
#define SQLITE_STORAGE_H
#include <iostream>
#include<fstream>
#include <optional>
#include <string>
#include "storage.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include "user.h"
using namespace std;

class SqliteStorage:public Storage
{
protected:
    QSqlDatabase db;
public:
    SqliteStorage(const string &dir_name);
    bool isOpen() const;
    bool open();
    void close();

    vector<Cat> getAllCats();
    optional<Cat> getCatById(int cat_id);
    bool updateCat(const Cat & cat);
    bool removeCat(int cat_id);
    int insertCat(const Cat & cat);

    vector<Breed> getAllBreeds();
    optional<Breed> getBreedById(int breed_id);
    bool updateBreed(const Breed & breed);
    bool removeBreed(int breed_id);
    int insertBreed(const Breed & breed);

    int registerUser(QString const & username,QString const & password, QString const & fullname);
    optional<User> getUserAuth(QString const & username, QString const & password);
    vector<Cat> getAllUserCats(int user_id);

    vector<Breed> getAllCatBreeds(int cat_id);
    bool insertCatBreeds(int cat_id,int breed_id);
    bool removeCatBreed(int cat_id,int breed_id);
    vector<Cat> search(QString const & s,int use_id,int number,int miss);
    int number(QString const & s,int use_id);
};

#endif // SQLITE_STORAGE_H
