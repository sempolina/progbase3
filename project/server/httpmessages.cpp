#include "httpmessages.h"
#include<QDebug>
using namespace std;
const char  * HttpContentTypeHeader ="Content-Type";
const char * HttpContentLenghtHeader="Content-Lenght";
QString HttpRequest::contentType()const
{
    if(!headers.contains(HttpContentTypeHeader))
        return "";
    return headers [HttpContentTypeHeader];

}
int HttpRequest::contentLenght() const
{
    if(!headers.contains(HttpContentLenghtHeader))
        return 0;
    return headers[HttpContentLenghtHeader].toInt();
}
void HttpResponse::setContentType(const QString & type)
{
   headers[HttpContentTypeHeader]=type;

}
void HttpResponse::setContentLenght(int lenght)
{
 headers[HttpContentLenghtHeader]=QString::number(lenght);
}
void HttpResponse::setStatus(int code,const QString & description)
{
    status_code=code;
    status_description=description;
}
HttpRequest parseHttpRequest(const QString & str)
{
    HttpRequest res;
    QString copy=str;
    QTextStream ts{&copy};
    QString request_line =ts.readLine();
    QStringList request_line_parts=request_line.split(" ");
    res.method=request_line_parts[0];
    res.uri=request_line_parts[1];
    res.http_version=request_line_parts[2];
    QString header_line;
    while((header_line=ts.readLine()).length()>0)
    {
        QStringList parts=header_line.split(":");
        if(parts[0]=="Authorization")
        {
            QStringList parts2=parts[1].split(" ");
            parts[1]=parts2[1];
            QByteArray ba;
            ba.append(parts[1]);
            QString str=QByteArray::fromBase64(ba);
            QStringList parts3=str.split(":");
            res.user=parts3[0];
            res.password=parts3[1];
        }
        res.headers[parts[0]]=parts[1];
    }
    res.body =ts.readAll();
    return res;
}
QString formatHttpResponse(const HttpResponse & res)
{
   QString str;
   QTextStream ts {&str};
   ts<<res.http_version<<" "<<res.status_code<<res.status_description<<"\r\n";
   QMap <QString,QString>::const_iterator it;
   for(it=res.headers.cbegin(); it != res.headers.cend();++it)
        ts<<it.key()+":"+it.value()<<"\r\n";
   ts<<"\r\n";
   ts<<res.body;
   return ts.readAll();
}
QMap<QString,QString> parseUrlQuery(const QString & uri)
{
    QMap<QString,QString> query;
    if(uri.contains('?'))
    {
        QUrlQuery url_query{uri.split("?")[1]};
        QList<QPair<QString,QString>> pairs=url_query.queryItems();
        for(QPair<QString,QString> & pair: pairs)
        {
            query[pair.first]=pair.second;
        }
    }
    return query;
}
bool patternMatch(const QString & pattern,const QString & path,QMap<QString,QString> & params)
{
    const QRegExp paramNameRegExp{":\\w+"};
    QRegExp paramNamesRegExp{QString{pattern}.replace(paramNameRegExp,":([^/]+)")};
    QRegExp paramValuesRegExp{QString{pattern}.replace(paramNameRegExp,":([^/]+)")};
    if(!paramValuesRegExp.exactMatch(path))
        return false;
    paramNamesRegExp.exactMatch(pattern);
    for(int i=0;i<paramNamesRegExp.captureCount();i++)
    {
        QString  paramName=paramNamesRegExp.cap(i+1);
        QString paramValue=paramValuesRegExp.cap(i+1);
        params[paramName]=paramValue;
    }
    return true;
}
