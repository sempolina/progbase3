#include "httpserver.h"
using namespace std;
using HttpHandler=function<void(HttpRequest & req, HttpResponse & res)>;
HttpServer::HttpServer(QObject *parent) : QObject(parent)
{
   QObject::connect(&tcp_server, &QTcpServer::acceptError,this,&HttpServer::onError);
   QObject::connect(&tcp_server,&QTcpServer::newConnection,this,&HttpServer::onNewConnection);
}
HttpResponse HttpServer::handleRequest(HttpRequest & req)
{
    HttpResponse res;
    res.http_version="HTTP/1.1";
    HttpHandler handler = getHandler(req);
    if(handler_!=nullptr)
        handler=handler_;
    if(handler==nullptr)
    {
        res.setStatus(404,"Not Found");
    }
    else
    {
        req.query=parseUrlQuery(req.uri);
        res.setStatus(200,"OK");
        res.setContentType("text/json");
        handler(req,res);
    }
    res.setContentLenght(res.body.length());
    return res;
}
bool HttpServer::start (int port)
{
    return tcp_server.listen(QHostAddress::Any,port);
}
void HttpServer::onError(QAbstractSocket::SocketError socketError)
{
   qDebug()<<"tcp server error: "<<socketError;
}
void HttpServer::onNewConnection()
{
    QObject * sender =this->sender();
    QTcpServer *server =static_cast<QTcpServer *> (sender);
    qDebug()<<"got client new pending connection";
    QTcpSocket * new_client =server->nextPendingConnection();
    connect(new_client,&QTcpSocket::readyRead,this,&HttpServer::onClientReadyRead);
    connect(new_client,&QTcpSocket::bytesWritten,this,&HttpServer::onClientDataSent);
    connect(new_client,&QTcpSocket::disconnected,this,&HttpServer::onClientDisconnected);
}

void HttpServer::onClientReadyRead()
{
    QObject *sender=this->sender();
    QTcpSocket * client =static_cast<QTcpSocket*>(sender);
    QByteArray recieved_data = client->readAll();
    QString recieved_string=QString::fromUtf8(recieved_data);
    qDebug()<<"data received from client:>>>"<<recieved_string<<"<<<";
     if(current_request.contentLenght()==0)
        current_request=parseHttpRequest(recieved_string);

     else
         current_request.body=recieved_string;
     if(current_request.contentLenght()==0||current_request.body.length()>0)
     {
         HttpResponse res=handleRequest(current_request);
         client->write(formatHttpResponse(res).toUtf8());
         client->flush();
         current_request=HttpRequest{};
     }
}
void HttpServer::onClientDataSent()
{
    qDebug()<<"data sent to client.";
    QObject * sender =this->sender();
    QTcpSocket *client =static_cast<QTcpSocket *>(sender);
    client->close();
}
void HttpServer::onClientDisconnected()
{
    QObject * sender =this->sender();
    QTcpSocket * client = static_cast <QTcpSocket*>(sender);
    qDebug()<<"client disconnected";
    client->deleteLater();
    qDebug()<<"Hello";
}
HttpHandler HttpServer::getHandler(HttpRequest & req)
{
   QString path =req.uri.split("?")[0];
   HttpHandler handler = nullptr;
   QMap<QString,HttpHandler>::iterator it;
   for(it=handlers_.begin();it!=handlers_.end();++it)
   {
       if(patternMatch(it.key(),req.method+" "+path,req.params))
       {
           handler=it.value();
           break;
       }
   }
   return handler;
}
