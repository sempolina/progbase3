#ifndef HTTPSERVER_H
#define HTTPSERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QDebug>
#include "httpmessages.h"
#include "json_parser.h"
#include <QHostAddress>
#include"sqlite_storage.h"
using namespace std;
class HttpServer : public QObject
{
    QTcpServer tcp_server;
    HttpRequest current_request;
    HttpResponse handleRequest(HttpRequest & req);
    using HttpHandler=function<void(HttpRequest & req, HttpResponse & res)>;
    HttpHandler handler_=nullptr;
    QMap<QString,HttpHandler> handlers_;
    HttpHandler getHandler(HttpRequest & req);

    Q_OBJECT
public:
    explicit HttpServer(QObject *parent = nullptr);
    void setHandler(const HttpHandler & handler)
    {
        handler_=handler;
    }

    bool start (int port);
    void get(const QString & uri, const HttpHandler & handler){handlers_["GET"+uri]=handler;}
    void post(const QString & uri, const HttpHandler & handler){handlers_["GET"+uri]=handler;}
    void put(const QString & uri, const HttpHandler & handler){handlers_["GET"+uri]=handler;}
    void delete_(const QString & uri, const HttpHandler & handler){handlers_["GET"+uri]=handler;}
signals:

public slots:
private slots:
    void onError(QAbstractSocket::SocketError socketError);
    void onNewConnection();

    void onClientReadyRead();
    void onClientDataSent();
    void onClientDisconnected();
};

#endif // HTTPSERVER_H
